# Firebase


## Cómo agregar Firebase a tu proyecto de JavaScript
### Agrega Firebase a tu app
Para agregar Firebase a tu app, necesitarás un proyecto de Firebase, el SDK de Firebase y un fragmento corto del código de inicialización que incluya algunos detalles sobre tu proyecto.

1. Crea un proyecto en [Firebase Console](https://console.firebase.google.com/).
2. Selecciona la opción para **agregar Firebase a tu aplicación web**.
3. Agrega el fragmento del código de inicialización, que usarás en unos instantes.

---
**Nota:** Si necesitas obtener el código de configuración otra vez, haz clic en **Agregar app** en la página de descripción general y vuelve a seleccionar la opción web.

---

```javascript
<script src="https://www.gstatic.com/firebasejs/4.2.0/firebase-app.js"></script>

<script src="https://www.gstatic.com/firebasejs/4.2.0/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.2.0/firebase-database.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.2.0/firebase-messaging.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.2.0/firebase-storage.js"></script>

<script>
  // Initialize Firebase
  // TODO: Replace with your project's customized code snippet
  var config = {
    apiKey: "<API_KEY>",
    authDomain: "<PROJECT_ID>.firebaseapp.com",
    databaseURL: "https://<DATABASE_NAME>.firebaseio.com",
    storageBucket: "<BUCKET>.appspot.com",
    messagingSenderId: "<SENDER_ID>",
  };
  firebase.initializeApp(config);
</script>
```

### Usa servicios de Firebase

Una App de Firebase puede usar varios servicios de Firebase. Se puede acceder a cada servicio desde el espacio de nombres de **firebase**:

* **firebase.auth()**
* **firebase.storage()**
* **firebase.database()**

## Cómo comenzar con Firebase Authentication en sitios web

### Registra usuarios nuevos

Crea un formulario que permita a los usuarios nuevos registrarse en tu app mediante su dirección de correo electrónico y una contraseña. Cuando un usuario complete el formulario, valida la dirección de correo electrónico y la contraseña que proporcionó el usuario para después pasarlas al método **createUserWithEmailAndPassword**:
```javascript
firebase.auth().createUserWithEmailAndPassword(email, password).catch(function(error) {
  // Handle Errors here.
  var errorCode = error.code;
  var errorMessage = error.message;
  // ...
});
```

### Acceso de usuarios existentes

Crea un formulario que les permita a los usuarios existentes acceder con su dirección de correo electrónico y una contraseña. Cuando un usuario complete el formulario, llama al método **signInWithEmailAndPassword**:
```javascript
firebase.auth().signInWithEmailAndPassword(email, password).catch(function(error) {
  // Handle Errors here.
  var errorCode = error.code;
  var errorMessage = error.message;
  // ...
});
```

### Configura un observador de estado de autenticación y obtén datos del usuario

Para cada una de las páginas de la app que necesiten información sobre el usuario que accedió, vincula un observador al objeto de autenticación global. Se llama a este observador cada vez que el estado de acceso del usuario cambia.

Vincula al observador con el método **onAuthStateChanged**. Cuando un usuario acceda correctamente, podrás obtener información acerca de él en el observador.
```javascript
firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    // User is signed in.
    var displayName = user.displayName;
    var email = user.email;
    var emailVerified = user.emailVerified;
    var photoURL = user.photoURL;
    var isAnonymous = user.isAnonymous;
    var uid = user.uid;
    var providerData = user.providerData;
    // ...
  } else {
    // User is signed out.
    // ...
  }
});
```

## Cómo comenzar con Firebase Database en sitios web

### Configura reglas de Firebase Database
Realtime Database proporciona un lenguaje de reglas declarativas que te permite definir cómo se deben estructurar tus datos, cómo se deben indexar y cuándo se pueden leer y escribir. Según la configuración predeterminada, el acceso de lectura y escritura a tu base de datos es restringido, por lo que solo los usuarios autenticados pueden leer o escribir datos. Para comenzar sin configurar Authentication, puedes configurar tus reglas para el acceso público. Esto hace que tus datos estén abiertos para todos, incluso las personas que no usan la aplicación. Asegúrate de volver a restringir tu base de datos cuando configures la autenticación.


## Cómo leer y escribir datos en la Web

### Obtén una referencia a una base de datos

Para leer y escribir en la base de datos, necesitas una instancia de **firebase.database.Reference**:

```javascript
// Get a reference to the database service
var database = firebase.database();
```

### Operaciones básicas de escritura

Para ejecutar operaciones de escritura básicas, puedes usar **set()** para guardar datos en una referencia que especifiques y reemplazar los datos existentes en esa ruta de acceso. Por ejemplo, una aplicación social de blogs puede agregar un usuario con **set()** de la siguiente manera:

```javascript
function writeUserData(userId, name, email, imageUrl) {
  firebase.database().ref('users/' + userId).set({
    username: name,
    email: email,
    profile_picture : imageUrl
  });
}
```
Si usas **set()**, se sobrescriben los datos en la ubicación que especificas, incluidos todos los nodos secundarios.

### Lee los datos una sola vez

En algunos casos, deseas tener una instantánea de los datos sin detectar cambios, como cuando inicializas un elemento de IU que no debería cambiar. Puedes usar el método **once()** para simplificar esta situación: se activa una vez y no vuelve a activarse.

Esto resulta útil para los datos que solo se deben cargar una vez y que, según lo esperado, no cambiarán con frecuencia ni necesitarán una escucha activa. Por ejemplo, en la app de blogs de los ejemplos anteriores se usa este método para cargar el perfil de un usuario cuando este comienza a crear una publicación nueva:

```javascript
var userId = firebase.auth().currentUser.uid;
return firebase.database().ref('/users/' + userId).once('value').then(function(snapshot) {
  var username = (snapshot.val() && snapshot.val().username) || 'Anonymous';
  // ...
});

```

### Actualiza o borra datos

#### Actualiza campos específicos

Para escribir de forma simultánea en elementos secundarios específicos de un nodo sin sobrescribir otros nodos secundarios, usa el método **update()**.

Cuando llamas a **update()**, puedes especificar una ruta de acceso de la clave a fin de actualizar valores secundarios de nivel inferior. Si se almacenan datos en varias ubicaciones para obtener un escalamiento mejor, puedes actualizar todas las instancias de esos datos mediante la distribución de datos.

Por ejemplo, es posible que una app social de blogs cree una publicación y que esta se actualice de forma simultánea en el feed de actividad reciente y en el feed de actividad de publicaciones del usuario con un código como el siguiente:

```javascript
function writeNewPost(uid, username, picture, title, body) {
  // A post entry.
  var postData = {
    author: username,
    uid: uid,
    body: body,
    title: title,
    starCount: 0,
    authorPic: picture
  };

  // Get a key for a new Post.
  var newPostKey = firebase.database().ref().child('posts').push().key;

  // Write the new post's data simultaneously in the posts list and the user's post list.
  var updates = {};
  updates['/posts/' + newPostKey] = postData;
  updates['/user-posts/' + uid + '/' + newPostKey] = postData;

  return firebase.database().ref().update(updates);
}

```
Este ejemplo usa **push()** para crear una entrada en el nodo que contiene las entradas de todos los usuarios en **/posts/$postid** y recupera la clave de manera simultánea. Esta clave se puede usar para crear una segunda entrada en las publicaciones del usuario en **/user-posts/$userid/$postid**.

Con estas rutas de acceso, puedes ejecutar actualizaciones simultáneas en varias ubicaciones del árbol JSON con una única llamada a update(), de manera similar a este ejemplo en el que se crea la publicación nueva en ambas ubicaciones. Las actualizaciones simultáneas que se ejecutan de esta forma son atómicas: todas las actualizaciones se aplican correctamente o todas fallan.

#### Borra datos

La forma más sencilla de borrar datos es llamar a **remove()** en una referencia a la ubicación de los datos.

Para borrar, también puedes especificar **null** como el valor de otra operación de escritura, como **set()** o **update()**. Puedes usar esta técnica con **update()** para borrar varios elementos secundarios en una misma llamada de la API.

#### Recibe una Promise

Para saber en qué momento se confirman los datos en el servidor de Firebase Realtime Database, puedes usar una Promise. Tanto **set()** como **update()** pueden dar como resultado una **Promise** que puedes usar para saber cuándo se confirma la escritura en la base de datos.