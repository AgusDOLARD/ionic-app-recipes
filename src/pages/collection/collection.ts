import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { DatabaseserviceProvider } from './../../providers/databaseservice/databaseservice';
import { DishPage } from '../dish/dish';
/**
 * Generated class for the CollectionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-collection',
  templateUrl: 'collection.html',
})
export class CollectionPage {

  collectionName: string;
  cu;
  collection: Observable<any[]>;


  constructor(public navCtrl: NavController, public navParams: NavParams, public db: DatabaseserviceProvider) {
    this.collectionName = navParams.get('collectionKey');
    this.cu = this.db.getCurrentUser().uid;
    this.collection = this.db.getCollectionByName(this.collectionName, this.cu);
  }

  ionViewDidLoad() {
  }

  goToDish(key) {
    this.navCtrl.push(DishPage, {
      'key': key
    });
  }

}
