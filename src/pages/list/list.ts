import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

//import { Observable } from 'rxjs/Observable';

import { CollectionPage } from '../collection/collection';
import { DatabaseserviceProvider } from './../../providers/databaseservice/databaseservice';
import { AngularFireDatabase } from 'angularfire2/database';

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {

  collections = [];
  cu;

  constructor(
    public navCtrl: NavController,
    public fb: DatabaseserviceProvider,
    public db: AngularFireDatabase,
  ) {
    this.cu = this.fb.getCurrentUser().uid;
    this.fb.getCollections(this.fb.getCurrentUser().uid).subscribe(coll => {
      coll.forEach(e => {
        var k = JSON.stringify(e.key).split('"')[1];
        this.db.database.ref('users').child(this.cu).child('collections').child(k).limitToLast(1).once('value', ac => {
          let kk = Object.keys(ac.val())[0]
          let w = JSON.stringify(ac.val()[kk]['image']).split('"')[1];
          this.collections.push({ title: k, img: w })
        });
        
      })
    });
  }

  goToCollection(collection){
    console.log(collection)
    this.navCtrl.push(CollectionPage, {
      'collectionKey': collection
    });
  }
}
