import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SignUpPage } from '../sign-up/sign-up';

import { MyApp } from '../../app/app.component';
import { AngularFireAuth } from 'angularfire2/auth';
import { LoadingController } from 'ionic-angular';

import { Storage } from '@ionic/storage';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  email: string;
  pass: string;
  errors: string;

  emailL: string;
  usernameL: string;

  isenabled: boolean = false;
  logged: boolean = false;

  loggedUser: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public fa: AngularFireAuth,
    public loadingCtrl: LoadingController,
    private storage: Storage
  ) {
    this.loggedUser = this.fa.auth.currentUser;
    if (this.loggedUser) {
      this.logged = true;
      this.emailL = this.loggedUser.email;
      this.usernameL = this.loggedUser.displayName;
    }
  }

  goToSignUp() {
    this.navCtrl.push(SignUpPage);
  }

  enableButton() {
    if (this.email && this.pass) {
      this.isenabled = true;
    }
  }

  signIn() {
    const loading = this.loadingCtrl.create({
      content: 'Iniciando Sesion...'
    });
    loading.present();
    this.fa.auth.signInWithEmailAndPassword(this.email, this.pass).then((u) => {
      this.storage.set('e', this.email);
      this.storage.set('p', this.pass);
      this.navCtrl.push(MyApp);
    }).catch((e) => {
      this.errors = e.message;
    });
    loading.dismiss();
  }

  signOut() {
    this.fa.auth.signOut().then(() => {
      this.storage.remove('e');
      this.storage.remove('p');
      this.navCtrl.push(MyApp);
    }).catch((e) => {
      console.error(e.message);
    });
  }

  
  ionViewDidLoad() {
  }


}
