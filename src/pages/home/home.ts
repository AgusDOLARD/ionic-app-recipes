import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { Observable } from 'rxjs/Observable';
import { DishPage } from '../dish/dish';
import { AddDishPage } from '../add-dish/add-dish';
import { Events } from 'ionic-angular';
import { DatabaseserviceProvider } from './../../providers/databaseservice/databaseservice';
import { AngularFireDatabase } from 'angularfire2/database';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  init: Observable<any[]>;
  dishes: Observable<any[]>;
  showSearchBar: boolean = false;
  didLoad: boolean = false;
  isAdmin: boolean = false;
  cu;

  constructor(
    public navCtrl: NavController,
    public db: DatabaseserviceProvider,
    public events: Events,
    public fb: AngularFireDatabase
  ) {
    events.publish('checkIfAuth');
    this.init = this.db.getDishes();
    this.dishes = this.init;
    this.init.subscribe(() => {
      this.didLoad = true;
      this.isAdminCheck()
    })
  }

  searchBar($event) {
    var sb = this.showSearchBar;
    setTimeout(() => {
      if (sb) {
        this.showSearchBar = false
      } else {
        this.showSearchBar = true
      }
      $event.complete();
    }, 500);
  }

  search($event) {
    let q = $event.target.value;
    this.init.subscribe(dishes => {
      this.dishes = Observable.of(dishes.filter(dish => (
        dish.name.toUpperCase().indexOf(q.toUpperCase()) >= 0
      )))
    })
  }

  clearSearch() {
    this.dishes = this.init;
  }

  isAdminCheck() {
    try {
      this.cu = this.db.getCurrentUser();
      this.fb.database.ref('users').child(this.cu.uid).on('value', acc => {
        this.isAdmin = acc.val()['admin'];
      })
    } catch (e) {
      console.error(e.message)
      this.isAdmin = false;
    }
  }

  addDishPage(){
    this.navCtrl.push(AddDishPage)
  }

  goToDish(key) {
    this.navCtrl.push(DishPage, {
      'key': key
    });
  }
}