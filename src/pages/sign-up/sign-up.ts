import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DatabaseserviceProvider } from './../../providers/databaseservice/databaseservice';
import { AngularFireAuth } from 'angularfire2/auth';

import { MyApp } from '../../app/app.component';

import { LoadingController } from 'ionic-angular';


/**
 * Generated class for the SignUpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html',
})
export class SignUpPage {
  username: string;
  email: string;
  pass: string;
  passRP: string;

  errors: string;

  isenabled: boolean = false;


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public fb: DatabaseserviceProvider,
    public fa: AngularFireAuth,
    public loadingCtrl: LoadingController
  ) { }

  enableButton() {
    if (this.pass == this.passRP && this.pass) {
      if (this.username && this.email) {
        this.isenabled = true;
      }
    }
    else {
      this.isenabled = false;
    }
  }

  signUp() {
    const loading = this.loadingCtrl.create({
      content: 'Signing up...'
    });
    loading.present();
    this.fa.auth.createUserWithEmailAndPassword(this.email, this.pass).then((u) => {
      var cu = this.fa.auth.currentUser.uid;
      u.updateProfile({
        displayName: this.username
      })
      let data = {
        'username': this.username,
        'mail': this.email
      }
      this.fb.signUpUser(cu, data);
      this.navCtrl.push(MyApp);
    }).catch((e) => {
      this.errors = e.message;
    });
    loading.dismiss();
  }

  ionViewDidLoad() {
  }

}
