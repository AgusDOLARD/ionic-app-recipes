import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the AddDishPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-dish',
  templateUrl: 'add-dish.html',
})
export class AddDishPage {

  name: string;
  desc: string;
  ingredients = [];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {}

  newIngredient(){
    this.ingredients.push({});
  }

  addNewDish(){
    console.log(JSON.stringify(this.ingredients))
  }

}
