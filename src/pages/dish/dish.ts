import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DatabaseserviceProvider } from './../../providers/databaseservice/databaseservice';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { ActionSheetController } from 'ionic-angular'
import { AlertController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
//import { AngularFireObject } from 'angularfire2/database';

//import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';


/**
 * Generated class for the DishPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dish',
  templateUrl: 'dish.html',
})
export class DishPage {

  key;
  dish: Observable<any>;
  ingredients: Observable<any[]>;
  preparation: Observable<any[]>;
  collections: Observable<any[]>;
  cu;

  heart: { color: string, icon: string } = { color: 'white', icon: 'heart-outline' };


  constructor(
    public navCtrl: NavController,
    public actionSheetCtrl: ActionSheetController,
    public fb: AngularFireDatabase,
    public navParams: NavParams,
    public db: DatabaseserviceProvider,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController
  ) {
    this.key = navParams.get('key');
    this.db.updateRating(this.key);
    this.dish = this.db.getDish(this.key);
    this.ingredients = this.db.getIngredients(this.key);
    this.preparation = this.db.getPreparation(this.key);
    try {
      this.cu = this.db.getCurrentUser().uid
      this.collections = this.db.getCollections(this.cu)
    } catch (e) {
      console.error(e.code, e.message)
    }

  }

  ionViewDidLoad() {
  }

  alert() {
    try {
      var options = {
        title: 'Seleccionar coleccion',
        buttons: [
          {
            text: 'Crear nueva coleccion',
            icon: 'add',
            handler: () => {
              const alert = this.alertCtrl.create({
                title: 'Cree una nueva coleccion',
                inputs: [
                  {
                    name: 'collection',
                    placeholder: 'Coleccion'
                  }
                ],
                buttons: [
                  {
                    text: 'Cancelar',
                    role: 'cancel',
                  },
                  {
                    text: 'Agregar',
                    handler: data => {
                      this.like(data.collection.trim());
                    }
                  }
                ]
              });
              alert.present();
            }
          }
        ]
      }
      this.collections.subscribe(coll => {
        coll.forEach(e => {
          var k = JSON.stringify(e.key).split('"')[1];
          if (k != 'favourite') {
            var a = {
              text: k,
              icon: 'albums',
              handler: () => {
                this.like(k);
              }
            }
            options.buttons.unshift(a);
          }
        })
      });
      const alert = this.actionSheetCtrl.create(options);
      alert.present();
    } catch (e) {
      this.showToast();
      console.error(e.message)
    }
  }


  async like(collection: string) {
    try {
      const cu = this.db.getCurrentUser().uid;

      const ref = this.fb.database.ref('/users/').child(cu).child('collections').child(collection).child(this.key)
      const l = await ref.once("value")
        .then(function (snapshot) {
          var a = snapshot.exists();
          return a
        })

      if (cu) {
        if (!l) {
          this.db.likeDish(this.dish, cu, collection);
          this.heart = { color: 'danger', icon: 'heart' }
        } else {
          this.db.dislikeDish(this.key, cu, collection);
          this.heart = { color: 'white', icon: 'heart-outline' }
        }
      }
    } catch (e) {
      this.showToast();
      console.error(e.message)
    }
  }

  showToast() {
    let toast = this.toastCtrl.create({
      message: 'Inicia sesion',
      duration: 3000,
    });

    toast.present();
  }
}
