import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { DatabaseserviceProvider } from '../providers/databaseservice/databaseservice';
import { AngularFireAuth } from 'angularfire2/auth';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { LoginPage } from '../pages/login/login';

import { Storage } from '@ionic/storage';

import { Events } from 'ionic-angular';

@Component({
	templateUrl: 'app.html'
})
export class MyApp {
	@ViewChild(Nav) nav: Nav;

	rootPage: any = HomePage;

	pages: Array<{ title: string, icon: string, component: any }>;

	constructor(
		public platform: Platform,
		public statusBar: StatusBar,
		public splashScreen: SplashScreen,
		public fb: DatabaseserviceProvider,
		public fa: AngularFireAuth,
		public storage: Storage,
		public events: Events
	) {
		this.initializeApp();
		events.subscribe('checkIfAuth', () => {
			this.checkIfAuth();
		});

		// used for an example of ngFor and navigation
		this.pages;

	}

	initializeApp() {
		this.platform.ready().then(() => {
			// Okay, so the platform is ready and our plugins are available.
			// Here you can do any higher level native things you might need.
			this.statusBar.backgroundColorByHexString('00695C');
			this.splashScreen.hide();
		});
	}

	async checkIfAuth() {
		this.pages = [];
		this.pages = [
			{ title: 'Inicio', icon: 'home', component: HomePage }
		];
		let log = await this.storage.get('e');
		if (log) {
			this.pages.push({ title: 'Colecciones', icon: 'albums', component: ListPage })
			this.pages.push({ title: 'Sesion Iniciada', icon: 'person', component: LoginPage });
		} else {
			this.pages.push({ title: 'Iniciar Sesion', icon: 'logo-googleplus', component: LoginPage });
		}
	}

	openPage(page) {
		// Reset the content nav to have just this page
		// we wouldn't want the back button to show in this scenario
		switch (page.component) {
			case LoginPage: {
				this.nav.push(LoginPage);
				break;
			}
			default: {
				this.nav.setRoot(page.component);
				break;
			}
		}

	}
}
