import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { Storage } from '@ionic/storage';
//import { FirebaseApp } from 'angularfire2';

/*
  Generated class for the DatabaseserviceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DatabaseserviceProvider {

  constructor(
    public http: Http,
    public fb: AngularFireDatabase,
    public fa: AngularFireAuth,
    private storage: Storage
  ) { }

  getCurrentUser() {
    const cu = this.fa.auth.currentUser;
    if (cu) {
      return cu;
    } else {
      return null;
    }
  }

  getDishes() {
    return this.fb.list('/dishes').valueChanges();
  }

  async logIn() {
    var email: string;
    var pass: string;
    await this.storage.get('e').then((e) => {
      email = e
    });
    await this.storage.get('p').then((p) => {
      pass = p
    });
    if (pass && email) {
      await this.fa.auth.signInWithEmailAndPassword(email, pass).then((res) => {
        console.log('Logued User:', res.email);
        return true;
      }).catch((e) => {
        console.error(e.message);
        return false;
      });
    } else {
      console.log('No User Logged In');
      return false;
    }
  }

  signUpUser(uid: string, data: any) {
    this.fb.database.ref('users').child(uid).set(data);
  }

  getDish(key: string) {
    return this.fb.object('dishes/' + key).valueChanges();
  }

  getIngredients(key: string) {
    return this.fb.list('dishes/' + key + '/ingredients').valueChanges();
  }

  getPreparation(key: string) {
    return this.fb.list('dishes/' + key + '/preparation/txt').valueChanges();
  }

  updateRating(key: string): any {
    var ratings: Number[] = [];
    var res: number = 0;
    var am: number;
    var cant: number = 0;
    var rat: number;
    const rating = this.fb.list('dishes/' + key + '/ratings').valueChanges();
    rating.subscribe(snapshots => {
      snapshots.forEach((snapshot, i) => {
        ratings.push(new Number(snapshot));
      });
      for (var r in ratings) {
        let ra = Number(r);
        am = Number(ratings[ra]);
        cant += am;
        ra++;
        res += ra * am;
        rat = res / cant;
      }
      this.fb.database.ref('dishes').child(key).update({ 'rating': Math.round(rat * 10) / 10 });
    });
  }

  likeDish(dish: Observable<any>, user, coll) {
    dish.subscribe(e => {
      this.fb.database.ref('/users/').child(user).child('collections').child(coll).child(e.key).set({
        title: e.name,
        key: e.key,
        image: e.image
      });
    })

  }

  dislikeDish(dish, user, coll) {
    this.fb.database.ref('/users/').child(user).child('collections').child(coll).child(dish).remove();
  }


  getCollections(user) {
    return this.fb.list('/users/' + user + '/collections/').snapshotChanges()
  }

  getCollectionByName(collectionName, user) {
    return this.fb.list('/users/' + user + '/collections/' + collectionName).valueChanges()
  }

  addDish(name: String, desc: String, img: any, ingredients: any, preparation: any) {
    const dish = this.fb.database.ref('dishes').push();
    dish.set({
      'key': dish.key,
      'name': name,
      'desc': desc,
      'image': img,
      'ingredients': ingredients,
      'preparation': {
        'txt': preparation
      },
      'rating': 0,
      'ratings': {
        'a': 0,
        'b': 0,
        'c': 0,
        'd': 0,
        'e': 0
      },
      'timestamp': Math.floor(Date.now())
    });
  }

}